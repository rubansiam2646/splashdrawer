import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Page from './components/Page';
import Login from './components/Login';
import Signup from './components/Signup';
import Flat from './components/Flat';
import 'react-native-gesture-handler';
import { createDrawerNavigator } from '@react-navigation/drawer';

import splash from './components/splash';



const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();

function MyDrawer() {
  return (
    <Drawer.Navigator initialRouteName="page" >
      <Drawer.Screen name="Login" component={Login} />
      <Drawer.Screen name="Signup" component={Signup} />
      
    </Drawer.Navigator>
  );
}






export default function () {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="splash">
        <Stack.Screen
          name="splash"
          component={splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Page"
          component={Page}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={MyDrawer}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Signup"
          component={MyDrawer}
          options={{headerShown: false}}
        />
        
        
        
        <Stack.Screen name="Flat" component={Flat} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({});
