import React from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, Image, TouchableOpacity, ImageBackground,} from 'react-native';
const DATA = [
  {
    id: '1',
    title: 'facebook',
    image:require('../assests/fb.png'),
  },
   {
    id: '2',
    title: 'whatsapp',
    image:require('../assests/whats.png'),
  },
  {
    id: '3',
    title: 'instagram',
    image:require('../assests/insta.png'),
  }, 
  
  
];
const Flat = () => {
  const RenderItem = ({ DATA }) =>{
    return(
      <View style={styles.item}>
      <TouchableOpacity activeOpacity={0.8}>
        <ImageBackground source={DATA.image}
         style={{
           margin:10,
           width:260,
           height:215,
           alignItems:'center',
         }}>
        <Text style={{
           fontWeight:'bold',
           color:'red',
           fontSize:20,
         }} >{DATA.title}</Text></ImageBackground>
      </TouchableOpacity>
      </View>
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        style={styles.flat}
        data={DATA}
        renderItem={({item}) => < RenderItem DATA ={item}/>}
      />
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container:{
    flex: 1,
    alignItems:'center',
  },
  item: {
    margin:10,
    borderRadius:5,
    marginTop: 10,
    height:250,
    width:270, 
    marginBottom:10,
    alignItems:'center',
    padding:20,
    borderWidth:1,
    borderColor:'black'
  },
  
});
export default Flat;